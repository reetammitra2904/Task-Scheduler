package com.loadschedulerclient.taskprocessor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.loadschedulerclient.reader.TaskMapper;

public class TaskProcessor {

	private static TaskProcessor taskProcessor = null;
	

	
	private TaskProcessor() {
		
	}
	
	public static TaskProcessor getInstance() {
		if (taskProcessor == null) {
			taskProcessor = new TaskProcessor();
		}
		return taskProcessor;
	}
	
	public boolean processTask(String taskName) {
		String query = TaskMapper.taskMap.get(taskName);
		long sleepTime = new Double(Math.random()*10000).longValue();
		try {
			Thread.sleep(sleepTime);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	

}
