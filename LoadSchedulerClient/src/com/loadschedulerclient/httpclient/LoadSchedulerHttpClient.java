package com.loadschedulerclient.httpclient;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.loadschedulerclient.model.Processor;
import com.loadschedulerclient.model.Task;

public class LoadSchedulerHttpClient {
	
	private HttpClient httpClient = HttpClientBuilder.create().build();
	
	private static LoadSchedulerHttpClient loadSchedulerHttpClient = null;
	
	private LoadSchedulerHttpClient() {
		
	}
	
	public static LoadSchedulerHttpClient getInstance() {
		if (loadSchedulerHttpClient == null) {
			loadSchedulerHttpClient = new LoadSchedulerHttpClient();
		}
		return loadSchedulerHttpClient;
	}
	
	public void sendTaskAcknowledgement(Task task) {
		HttpPost postRequest = new HttpPost("http://10.42.0.118:8080/Load_Scheduler_Server/taskCompletion");
		postRequest.setHeader("User-Agent", "Mozilla/4.0");
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			String taskJson = mapper.writeValueAsString(task);
			urlParameters.add(new BasicNameValuePair("task", taskJson));
			postRequest.setEntity(new UrlEncodedFormEntity(urlParameters));
			HttpResponse response = httpClient.execute(postRequest);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void registerProcessor() {
		HttpPost postRequest = new HttpPost("http://10.42.0.118:8080/Load_Scheduler_Server/registerProcessor");
		Processor processor = new Processor();
		HashMap<String, Integer> taskCostMap = new HashMap<String, Integer>();
		InetAddress ipAddress = null;
		try {
			ipAddress = InetAddress.getLocalHost();
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			processor.setIpAddress(ipAddress.getLocalHost().getHostAddress().toString());
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader("/home/reets/DCProject/Task-Scheduler/LoadSchedulerClient/resources/TaskCost.csv"));
			String line = "";
			while ((line = bufferedReader.readLine()) != null) {
				String[] tokens = line.split("\\,");
				taskCostMap.put(tokens[0], Integer.parseInt(tokens[1]));
			}
			processor.setTaskCostMap(taskCostMap);
			bufferedReader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		postRequest.setHeader("User-Agent", "Mozilla/4.0");
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			String processorJson = mapper.writeValueAsString(processor);
			urlParameters.add(new BasicNameValuePair("processor", processorJson));
			postRequest.setEntity(new UrlEncodedFormEntity(urlParameters));
			HttpResponse response = httpClient.execute(postRequest);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
