package com.loadschedulerclient.reader;

import com.loadschedulerclient.httpclient.LoadSchedulerHttpClient;

public class StartUpThread implements Runnable{

	@Override
	public void run() {
		new TaskMapper();
		LoadSchedulerHttpClient.getInstance().registerProcessor();		
	}

}
