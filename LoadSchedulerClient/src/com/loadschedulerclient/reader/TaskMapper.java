package com.loadschedulerclient.reader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;



public class TaskMapper {
	
	public static HashMap<String, String> taskMap = new HashMap<String, String>();
	
	public TaskMapper() {
		init();
	}
	
	private void init() {
		mapTasks();
	}
	
	private void mapTasks() {
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader("/home/reets/DCProject/Task-Scheduler/"
					+ "LoadSchedulerClient/resources/TaskMap.csv"));
			String line = "";
			while ((line = bufferedReader.readLine()) != null) {
				String[] tokens = line.split("\\,");
				taskMap.put(tokens[0], tokens[1]);
			}
			bufferedReader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   System.out.println(taskMap.get("t1"));
	}
	
	

}
