package com.loadschedulerclient.model;

import java.util.HashMap;

public class Processor {
	
	private String ipAddress;
	private HashMap<String, Integer> taskCostMap;
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public HashMap<String, Integer> getTaskCostMap() {
		return taskCostMap;
	}
	public void setTaskCostMap(HashMap<String, Integer> taskCostMap) {
		this.taskCostMap = taskCostMap;
	}
	
	
	

}
