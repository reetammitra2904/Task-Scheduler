package com.loadschedulerclient.service;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.loadschedulerclient.httpclient.LoadSchedulerHttpClient;
import com.loadschedulerclient.model.Task;
import com.loadschedulerclient.taskprocessor.TaskProcessor;

import org.codehaus.jackson.map.ObjectMapper;


public class TaskRequestService extends HttpServlet{

public static ArrayList<Task> tasksList = new ArrayList<Task>();
	
	public void doPost(HttpServletRequest request,
            HttpServletResponse response)
    throws ServletException, IOException
{
		String taskJSON = request.getParameter("task");
		ObjectMapper mapper = new ObjectMapper();
		Task task = mapper.readValue(taskJSON, Task.class);
		TaskProcessor.getInstance().processTask(task.getTaskName());
		LoadSchedulerHttpClient.getInstance().sendTaskAcknowledgement(task);
}
	
	

}
