package com.loadschedulerserver.databaseconnection;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;



public class DatabaseConnection {
	
	public static Connection connection = null;
	
	public static Connection getConnection() {
		
		if (connection == null) {
			try {
				System.out.println("Creating connection");
				Class.forName("com.mysql.jdbc.Driver");
				connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/loadscheduler","root", "password");
			    System.out.println("Connection extablished");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return connection;
	}

}
