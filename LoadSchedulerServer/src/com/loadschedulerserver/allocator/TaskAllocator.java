package com.loadschedulerserver.allocator;

import java.util.ArrayList;
import java.util.HashMap;

import com.loadschedulerserver.client.LoadSchedulerClient;
import com.loadschedulerserver.model.Task;
import com.loadschedulerserver.model.TaskCount;
import com.loadschedulerserver.reader.TaskQueueReader;
import com.loadschedulerserver.sql.TaskSQL;
import com.loadschedulerserver.sql.ProcessorCostSQL;
import com.loadschedulerserver.sql.UpdateTasksSQL;

public class TaskAllocator implements Runnable{
	
	public static ArrayList<String> freeProcessorsList = new ArrayList<String>();
	public static HashMap<String, ArrayList<Task>> processorTaskMap = new HashMap<String, ArrayList<Task>>();
	public static HashMap<String, Double> engagedProcessorMap = new HashMap<String, Double>();
	public static ArrayList<Task> pendingTasksList = new ArrayList<Task>();
	public static int internalTaskId = 1;
	private double minCost = 0;
	public static boolean isProcessorsFree = true;
	public static boolean isFinalTask = false;
	public static HashMap<String, ArrayList<TaskCount>> processorTaskCountMap = new HashMap<String, ArrayList<TaskCount>>();
	
	public TaskAllocator() {
		allocateTask();
	}
	
	private void allocateTask() {
		System.out.println("In allocate task");
		System.out.println("The freeProcessorsList size is "+freeProcessorsList.size());
		ArrayList<Task> taskScheduleList = TaskQueueReader.taskScheduleList;
		for (int index = 0; index < taskScheduleList.size(); index++) {
			if (index == taskScheduleList.size() -1) {
				isFinalTask = true;
			}
			Task task = taskScheduleList.get(index);
			task.setTaskId(internalTaskId);
			task.setTaskName(task.getTaskName());
			TaskSQL.getInstance().insertTask(task);
			internalTaskId++;
			if (task.getDependentTasks() != null) {
				System.out.println("Inside dependentTasks");
				pendingTasksList.add(task);
				ArrayList<String> dependentTasksList = task.getDependentTasks();
				for (int dependentTasksListIndex = 0; dependentTasksListIndex < dependentTasksList.size(); dependentTasksListIndex++) {
					Task dependentTask = new Task();
					dependentTask.setTaskId(internalTaskId);
					dependentTask.setParentTaskId(task.getTaskId());
					dependentTask.setParentTaskName(task.getTaskName());
					dependentTask.setTaskName(dependentTasksList.get(dependentTasksListIndex));
					internalTaskId++;
					System.out.println("The dependent task is "+dependentTask.getTaskId());
					dependentTask.setDependentTasks(null);
					TaskSQL.getInstance().insertTask(dependentTask);
					String processorId = getMinCostProcessor(dependentTask);
					dependentTask.setCost(minCost);
					dependentTask.setProcessorId(processorId);
                    Thread loadSchedulerClientThread = new Thread(new LoadSchedulerClient(processorId, dependentTask));
                    loadSchedulerClientThread.start();
					System.out.println("Thread Execution Resumes");
					if (freeProcessorsList.contains(processorId)) {
						freeProcessorsList.remove(processorId);
						engagedProcessorMap.put(processorId, minCost);
					} else {
						engagedProcessorMap.put(processorId, engagedProcessorMap.get(processorId)+minCost);
					}
					    
						minCost = 0;
						if (processorTaskMap.get(processorId) == null) {
							ArrayList<Task> taskList = new ArrayList<Task>();
							taskList.add(dependentTask);
							processorTaskMap.put(processorId, taskList);
						} else {
							ArrayList<Task> taskList = processorTaskMap.get(processorId);
							taskList.add(dependentTask);
							processorTaskMap.put(processorId, taskList);
						}
						UpdateTasksSQL.getInstance().updateTasksTable(dependentTask, processorId);					
				}
				
				
			} else {
				String processorId = getMinCostProcessor(task);
				task.setCost(minCost);
				task.setProcessorId(processorId);
				Thread loadSchedulerClientThread = new Thread(new LoadSchedulerClient(processorId, task));
                loadSchedulerClientThread.start();
				if (freeProcessorsList.contains(processorId)) {
					freeProcessorsList.remove(processorId);
					engagedProcessorMap.put(processorId, minCost);
				} else {
					engagedProcessorMap.put(processorId, engagedProcessorMap.get(processorId)+minCost);
				}
				minCost = 0;
				if (processorTaskMap.get(processorId) == null) {
					ArrayList<Task> taskList = new ArrayList<Task>();
					taskList.add(task);
					processorTaskMap.put(processorId, taskList);
				} else {
					ArrayList<Task> taskList = processorTaskMap.get(processorId);
					taskList.add(task);
					processorTaskMap.put(processorId, taskList);
				}
				UpdateTasksSQL.getInstance().updateTasksTable(task, processorId);
			}
		}
		
	}
	
	private String getMinCostProcessor(Task task) {
		String minCostProcessorId = "";
		String taskName = task.getTaskName();
		HashMap<String, Double> processorMap = new HashMap<String, Double>();
		if (freeProcessorsList.size() > 0) {
			for (int index = 0; index < freeProcessorsList.size(); index++) {
				String processorId = freeProcessorsList.get(index);
				double cost = ProcessorCostSQL.getInstance().getCost(processorId, taskName);
				processorMap.put(processorId, cost);
			}
			ArrayList<String> engagedProcessorsList = new ArrayList<String>(engagedProcessorMap.keySet());
			for (int index = 0; index < engagedProcessorsList.size(); index++) {
				String processorId = engagedProcessorsList.get(index);
				double cost = ProcessorCostSQL.getInstance().getCost(processorId, taskName);
				processorMap.put(processorId, engagedProcessorMap.get(processorId)+cost);
			}
			ArrayList<String> processorList = new ArrayList<String>(processorMap.keySet());
			for (int index = 0; index < processorList.size(); index++) {
				if (index == 0) {
					minCostProcessorId = processorList.get(index);
					minCost = processorMap.get(minCostProcessorId);
				} else {
					double cost = processorMap.get(processorList.get(index));
					if (cost < minCost) {
						minCost = cost;
						minCostProcessorId = processorList.get(index);
					}
				}
			}
		} else {
			isProcessorsFree = false;
			while (freeProcessorsList.size() == 0) {
				System.out.println("The processors list is 0");
			}
			isProcessorsFree = true;
			minCostProcessorId = getMinCostProcessor(task);
		}
		System.out.println("The task is "+taskName+" and the processor is "+minCostProcessorId+" and the minCost "
				+ "is "+minCost);
		return minCostProcessorId;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		allocateTask();
	}
	
	
	
	

}
