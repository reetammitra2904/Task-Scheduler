package com.loadschedulerserver.allocator;

import java.util.ArrayList;
import java.util.HashMap;

import org.omg.CORBA.FREE_MEM;

import com.loadschedulerserver.model.Task;
import com.loadschedulerserver.sql.ExecutionCostSQL;
import com.loadschedulerserver.sql.ProcessorCostSQL;
import com.loadschedulerserver.sql.UpdateTasksSQL;



public class PendingTaskAllocator implements Runnable{
	

	private String parentTaskName = null;
	private int parentTaskId = 0;
	private Task task = null;
	private String processorId = "";
	
	
	public PendingTaskAllocator(String processorId, String parentTaskName, int parentTaskId, Task task) {
		this.parentTaskName = parentTaskName;
		this.parentTaskId = parentTaskId;
		this.task = task;
		this.processorId = processorId;
	}
	
	private void allocatePendingTasks(int parentTaskId) {
	  	HashMap<String, Double> engagedProcessorsMap = TaskAllocator.engagedProcessorMap;
	  	HashMap<String, Double> processorsMap = new HashMap<String, Double>(engagedProcessorsMap);
	  	for (int index = 0; index < TaskAllocator.freeProcessorsList.size(); index++) {
	  		processorsMap.put(TaskAllocator.freeProcessorsList.get(index), 0.0);
	  	}
	  	double minCost = 0;
	  	String minCostProcessorId = "";
	  	ArrayList<String> processorIdList = new ArrayList<String>(processorsMap.keySet());
	  	for (int index = 0; index < processorIdList.size(); index++) {
	  		if (index == 0) {
	  			minCost = processorsMap.get(processorIdList.get(index)) + 
		  				ProcessorCostSQL.getInstance().getCost(processorIdList.get(index), parentTaskName);
	  			minCostProcessorId = processorIdList.get(index);
	  		} else {
	  			double cost = processorsMap.get(processorIdList.get(index)) + 
		  				ProcessorCostSQL.getInstance().getCost(processorIdList.get(index), parentTaskName);
	  			if (cost < minCost) {
	  				minCost = cost;
	  				minCostProcessorId = processorIdList.get(index);
	  			}
	  		}
	  		
	  		
	  	}
	  	task.setCost(minCost);
	  	task.setProcessorId(minCostProcessorId);
	  	if (TaskAllocator.engagedProcessorMap.containsKey(minCostProcessorId)) {
	  		TaskAllocator.engagedProcessorMap.put(minCostProcessorId, TaskAllocator.engagedProcessorMap.get(minCostProcessorId)+minCost);
	  	} 
	  	ArrayList<Task> taskList = TaskAllocator.processorTaskMap.get(minCostProcessorId);
	  	if (taskList != null ) {
	  		taskList.add(task);
	  	} else {
	  		taskList = new ArrayList<Task>();
	  		taskList.add(task);
	  		
	  	}
	  	TaskAllocator.processorTaskMap.put(minCostProcessorId, taskList);
	  	removePendingTask(parentTaskId);
	  	UpdateTasksSQL.getInstance().updateTasksTable(task, minCostProcessorId);
	  	if (TaskAllocator.engagedProcessorMap.get(processorId) == 0) {
	  		TaskAllocator.engagedProcessorMap.remove(processorId);
	  		TaskAllocator.freeProcessorsList.add(processorId);
	  	}
	  	
	}

	@Override
	public void run() {
		allocatePendingTasks(parentTaskId);
		
	}
	
	private void removePendingTask(int taskId) {
		for (int index = 0; index < TaskAllocator.pendingTasksList.size(); index++) {
			Task task = TaskAllocator.pendingTasksList.get(index);
			if (task.getTaskId() ==  taskId) {
				TaskAllocator.pendingTasksList.remove(index);
				break;
			}
		}
	}

}
