package com.loadschedulerserver.reader;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.loadschedulerserver.allocator.PendingTaskAllocator;
import com.loadschedulerserver.allocator.TaskAllocator;


public class StartupJob implements ServletContextListener{
	
	
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("Context Initialized");
		//new ExecutionMatrixReader();
		Thread startupThread = new Thread(new StartUpThread());
		startupThread.start();
	}
	
	
}
