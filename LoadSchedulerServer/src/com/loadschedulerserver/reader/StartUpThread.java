package com.loadschedulerserver.reader;

import com.loadschedulerserver.allocator.TaskAllocator;

public class StartUpThread implements Runnable{
	
	public Thread taskAllocatorThread = new Thread(new TaskAllocator());

	@Override
	public void run() {
		// TODO Auto-generated method stub
		startThreads();
	}
	
	public void startThreads() {
		while (TaskAllocator.freeProcessorsList.size() < 1) {
			System.out.println("The number of free processors are "+TaskAllocator.freeProcessorsList.size());
		}
		new InterDependencyMatrixReader();
		new TaskQueueReader();
		taskAllocatorThread.start();
		System.out.println("Hello");
	}
	

}
