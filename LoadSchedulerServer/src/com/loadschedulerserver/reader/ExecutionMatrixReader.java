package com.loadschedulerserver.reader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import com.loadschedulerserver.sql.ExecutionCostSQL;
import com.loadschedulerserver.sql.TruncateExecutionCostSQL;

public class ExecutionMatrixReader {
	
	public static ArrayList<String> processorsList = new ArrayList<String>();
	
	public ExecutionMatrixReader() {
		insertCostsInDB();
	}
	
	public void insertCostsInDB() {
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader("/home/reets/DCProject/Task-Scheduler/"
					+ "LoadSchedulerServer/resources/ExecutionCostMatrix.csv"));
			String line = "";
			processorsList = new ArrayList<String>();
			int lineNumber = 1;
			TruncateExecutionCostSQL.getInstance().truncateExecutionCostTable();
			while ((line = bufferedReader.readLine()) != null) {
				if (lineNumber == 1) {
					String[] processorIds = line.split("\\,");
					for (int index = 1; index < processorIds.length; index++) {
						processorsList.add(processorIds[index]);
					}
				} else {
					String[] taskCost = line.split("\\,");
					String taskId = taskCost[0];
					for (int index = 1; index < taskCost.length; index++) {
						ExecutionCostSQL.getInstance().insertExecutionCost(processorsList.get(index-1), taskId, 
								Integer.parseInt(taskCost[index]));
					}
				}
				lineNumber++;
			}
			System.out.println("The size of processor list is "+processorsList.size());
			bufferedReader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
