package com.loadschedulerserver.reader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import com.loadschedulerserver.model.Task;

public class TaskQueueReader {
	
	public static ArrayList<Task> taskScheduleList = new ArrayList<Task>();
	
	public TaskQueueReader() {
		init();
	}
	
	public void init() {
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader("/home/reets/DCProject/Task-Scheduler/"
					+ "LoadSchedulerServer/resources/TaskQueues.csv"));
			String line = "";
			while ((line = bufferedReader.readLine()) != null) {
				String[] tasks = line.split("\\,");
				for (int index = 0; index < tasks.length; index++) {
					String taskId = tasks[index];
					Task task = new Task();
					task.setTaskName(taskId);
					String dependentTasks = InterDependencyMatrixReader.interdependencyMatrixMap.get(taskId);
					if (dependentTasks != null) {
						String[] dependentTasksTokens = dependentTasks.split("\\,");
						ArrayList<String> dependentTaskList = new ArrayList<String>();
						for (int dependentTasksTokensIndex = 0; dependentTasksTokensIndex < dependentTasksTokens.length; dependentTasksTokensIndex++) {
						   dependentTaskList.add(dependentTasksTokens[dependentTasksTokensIndex]);
						}						
						task.setDependentTasks(dependentTaskList);
					} else {
						task.setDependentTasks(null);
					}
					
					taskScheduleList.add(task);
				}
			}
			bufferedReader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

}
