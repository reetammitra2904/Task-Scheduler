package com.loadschedulerserver.reader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class InterDependencyMatrixReader {
	
	public static HashMap<String, String> interdependencyMatrixMap = new HashMap<String, String>();
	
	public InterDependencyMatrixReader() {
		init();
	}
	
	public void init() {
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader("//home/reets/DCProject/Task-Scheduler/"
					+ "LoadSchedulerServer/resources/InterdependencyMatrix.csv"));
			String line = "";
			int lineNumber = 1;
			ArrayList<String> taskIdsList = new ArrayList<String>();
			while ((line = bufferedReader.readLine()) != null) {
				if (lineNumber == 1) {
					String[] taskIds = line.split("\\,");
					for (int index = 1; index < taskIds.length; index++) {
						taskIdsList.add(taskIds[index].trim());
						interdependencyMatrixMap.put(taskIds[index].trim(), null);
					}
					
				} else {
					String[] tokens = line.split("\\,");
					String currentTask = tokens[0].trim();
					for (int index = 1; index < tokens.length; index++) {
						if (Integer.parseInt(tokens[index]) == 1) {
							System.out.println("The current task is "+currentTask+" "
									+ "and the dependent task is "+taskIdsList.get(index-1));
							if (interdependencyMatrixMap.get(currentTask) == null) {
								interdependencyMatrixMap.put(currentTask, taskIdsList.get(index-1));
							} else {
								String dependentTasks = interdependencyMatrixMap.get(currentTask);
								interdependencyMatrixMap.put(currentTask,dependentTasks+","+taskIdsList.get(index-1));
							}
						}
					}
				}
				lineNumber++;
			}
			bufferedReader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(interdependencyMatrixMap.get("t1"));
		System.out.println(interdependencyMatrixMap.get("t4"));
	}

}
