package com.loadschedulerserver.client;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.loadschedulerserver.model.Task;
import java.net.*;
import java.util.*;

public class LoadSchedulerClient implements Runnable{
	
	private static LoadSchedulerClient loadSchedulerClient = null;
	
	private HttpClient httpClient = HttpClientBuilder.create().build();
	
	private String processor = "";
	
	Task task = null;
	
	
	public LoadSchedulerClient(String processor, Task task) {
		this.processor = processor;
		this.task = task;
		
	}
	
	
	
	public void sendTasksToProcessor(String processor, Task task) {
		System.out.println("The processor is "+processor);
		HttpPost postRequest = new HttpPost("http://"+processor+":8080/LoadSchedulerClient/taskRequest");
		postRequest.setHeader("User-Agent", "Mozilla/4.0");
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			String taskJson = mapper.writeValueAsString(task);
			urlParameters.add(new BasicNameValuePair("task", taskJson));
			postRequest.setEntity(new UrlEncodedFormEntity(urlParameters));
			HttpResponse response = httpClient.execute(postRequest);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) throws Exception
    {
            System.out.println("Host addr: " + InetAddress.getLocalHost().getHostAddress());  // often returns "127.0.0.1"
            Enumeration<NetworkInterface> n = NetworkInterface.getNetworkInterfaces();
            for (; n.hasMoreElements();)
            {
                    NetworkInterface e = n.nextElement();
                    System.out.println("Interface: " + e.getName());
                    Enumeration<InetAddress> a = e.getInetAddresses();
                    for (; a.hasMoreElements();)
                    {
                            InetAddress addr = a.nextElement();
                            System.out.println("  " + addr.getHostAddress());
                    }
            }
            java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
            System.out.println("Hostname of local machine: " + localMachine.getHostName());
    }

	@Override
	public void run() {
		
		sendTasksToProcessor(processor, task);
	}

}
