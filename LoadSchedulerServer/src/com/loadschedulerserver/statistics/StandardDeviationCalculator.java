package com.loadschedulerserver.statistics;

import java.util.ArrayList;

public class StandardDeviationCalculator {
	
	private static StandardDeviationCalculator standardDeviationCalculator = null;
	
	private StandardDeviationCalculator() {
		
	}
	
	public static StandardDeviationCalculator getInstance() {
		if (standardDeviationCalculator == null) {
			standardDeviationCalculator = new StandardDeviationCalculator();
		}
		return standardDeviationCalculator;
	}
	
	public double calculateStandardDeviation(double mean, ArrayList<Double> inputValues) {
		double standardDeviation = 0;
		double total = 0;
		for (int index = 0; index < inputValues.size(); index++) {
			total = total + ((mean - inputValues.get(index))*(mean - inputValues.get(index)));
		}
		standardDeviation = Math.sqrt(total/inputValues.size());
		return standardDeviation;
	}

}
