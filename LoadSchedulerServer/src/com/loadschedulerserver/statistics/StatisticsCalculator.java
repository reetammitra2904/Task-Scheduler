package com.loadschedulerserver.statistics;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import com.loadschedulerserver.sql.FetchTaskTimeSQL;
import com.loadschedulerserver.sql.FetchZScoreSQL;
import com.loadschedulerserver.sql.UpdateExecutionCostSQL;

public class StatisticsCalculator {
	
	private static StatisticsCalculator statisticsCalculator = null;
	
	private StatisticsCalculator() {
		
	}
	
	public static StatisticsCalculator getInstance() {
		if (statisticsCalculator == null) {
			statisticsCalculator = new StatisticsCalculator();
		}
		return statisticsCalculator;
	}
	
	public void updateStatistics(String processorName, String taskName, int count) {
		ArrayList<Double> inputValuesList = FetchTaskTimeSQL.getInstance().fetchTaskTime(processorName, taskName);
		double mean = MeanCalculator.getInstance().calculateMean(inputValuesList);
		double median = MedianCalculator.getInstance().calculateMedian(inputValuesList)+count;
		double standardDeviation = StandardDeviationCalculator.getInstance().calculateStandardDeviation(mean, inputValuesList);
		double result = (median - mean)/standardDeviation;
		result = round(result, 2);
		double zScore = getZValue(result);
		if (zScore < 0.8) {
			updateStatistics(processorName, taskName, count+1);
		} else {
			UpdateExecutionCostSQL.getInstance().updateExecutionCost(processorName, taskName, median);
		}
	}
	
	private double getZValue(double result) {
	   double zScore = FetchZScoreSQL.getInstance().fetchZScore(Math.abs(result));
	   if (result >= 0) {
		   return zScore;
	   } else {
		   return 1-zScore;
	   }
       
	}
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();
	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	

}
