package com.loadschedulerserver.statistics;

import java.util.ArrayList;

public class MeanCalculator {
	
	private static MeanCalculator meanCalculator = null;
	
	private MeanCalculator() {
		
	}
	
	public static MeanCalculator getInstance() {
		if (meanCalculator == null) {
			meanCalculator = new MeanCalculator();
		}
		return meanCalculator;
	}
	
	
	public double calculateMean(ArrayList<Double> inputValuesList) {
		double mean = 0.0;
		double total = 0.0;
		for (int index = 0; index < inputValuesList.size(); index++) {
			total = total+inputValuesList.get(index);
		}
		mean = total/inputValuesList.size();
		return mean;
	}

}
