package com.loadschedulerserver.statistics;

import java.util.ArrayList;
import java.util.Collections;

public class MedianCalculator {
	
	private static MedianCalculator medianCalculator = null;
	
	private MedianCalculator() {
		
	}
	
	public static MedianCalculator getInstance() {
		if (medianCalculator == null) {
			medianCalculator = new MedianCalculator();
		}
		return medianCalculator;
	}
	
	public double calculateMedian(ArrayList<Double> inputValues) {
		Collections.sort(inputValues);
		Double inputValueSize = (double)inputValues.size();
		Double value = (inputValueSize/2);
		if (inputValueSize%2 == 0) {
			double inputValue1 = inputValues.get(value.intValue());
			double inputValue2 = inputValues.get(value.intValue()+1);
			return (inputValue1+inputValue2)/2;
		} else {
			return inputValues.get((int) Math.ceil(value));
		}
	}

}
