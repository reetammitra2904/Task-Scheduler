package com.loadschedulerserver.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.loadschedulerserver.databaseconnection.DatabaseConnection;

public class InsertZScore {
	
	public static void main(String[] args) {
		Connection connection = DatabaseConnection.getConnection();
		FileInputStream fileInputStream;
		try {
			fileInputStream = new FileInputStream("/home/reets/DCProject/Task-Scheduler/LoadSchedulerServer/resources/Z-TablePositive.xls");
			HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);
			HSSFSheet worksheet = workbook.getSheet("Postive");
			HSSFRow headerRow = worksheet.getRow(0);
			int rows = worksheet.getPhysicalNumberOfRows()-1;
			for (int index = 0; index < rows; index++) {
				HSSFRow row = worksheet.getRow(index+1);
				int numberOfColumns = row.getPhysicalNumberOfCells();
				for (int columnIndex = 1; columnIndex < numberOfColumns; columnIndex++) {
					double firstCellValue = row.getCell(0).getNumericCellValue();
					double columnIndexCellValue = headerRow.getCell(columnIndex).getNumericCellValue();
					double value = round(firstCellValue + columnIndexCellValue,2);
					double probability = 0.0;
					if (row.getCell(columnIndex).getCellType() == HSSFCell.CELL_TYPE_STRING) {
						probability = Double.parseDouble(row.getCell(columnIndex).getStringCellValue());
					} else {
						probability = row.getCell(columnIndex).getNumericCellValue();
					}
					
					PreparedStatement preparedStatement = connection.prepareStatement("insert into zScore values (?,?)");
					preparedStatement.setDouble(1, value);
					preparedStatement.setDouble(2, probability);
					preparedStatement.executeUpdate();
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}

}
