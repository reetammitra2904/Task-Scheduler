package com.loadschedulerserver.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;

import com.loadschedulerserver.databaseconnection.DatabaseConnection;

public class UpdateTaskCompletionSQL {
	
	private Connection connection = DatabaseConnection.getConnection();
	
	private static UpdateTaskCompletionSQL updateTaskCompletionSQL = null;
	
	private UpdateTaskCompletionSQL() {
		
	}
	
	public static UpdateTaskCompletionSQL getInstance() {
		if (updateTaskCompletionSQL == null) {
			updateTaskCompletionSQL = new UpdateTaskCompletionSQL();
		}
		return updateTaskCompletionSQL;
	}
	
	public void updateTaskCompletion(int taskId) {
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("update task set endTime = ? where taskId = ?");
			preparedStatement.setString(1, Calendar.getInstance().getTime().toString());
			preparedStatement.setInt(2, taskId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
	}

}
