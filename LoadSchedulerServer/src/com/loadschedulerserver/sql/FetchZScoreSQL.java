package com.loadschedulerserver.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.loadschedulerserver.databaseconnection.DatabaseConnection;

public class FetchZScoreSQL {
	
	private Connection connection = DatabaseConnection.getConnection();
	
	private static FetchZScoreSQL fetchZScoreSQL = null;
	
	private FetchZScoreSQL() {
		
	}
	
	public static FetchZScoreSQL getInstance() {
		if (fetchZScoreSQL == null) {
			fetchZScoreSQL = new FetchZScoreSQL();
		}
		return fetchZScoreSQL;
	}
	
	public double fetchZScore(double value) {
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("select probability from zScore where value = ?");
			preparedStatement.setDouble(1, value);
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				return resultSet.getDouble("probability");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0.0;
	}

}
