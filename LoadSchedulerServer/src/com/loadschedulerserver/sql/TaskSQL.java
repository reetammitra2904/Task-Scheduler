package com.loadschedulerserver.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;

import com.loadschedulerserver.databaseconnection.DatabaseConnection;
import com.loadschedulerserver.model.Task;

public class TaskSQL {
	
	private Connection connection = DatabaseConnection.getConnection();
	private static TaskSQL childTaskSQL = null;
	
	private TaskSQL() {
		
	}
	
	public static TaskSQL getInstance() {
		if (childTaskSQL == null) {
			childTaskSQL = new TaskSQL();
		}
		return childTaskSQL;
	}
	
	public void insertTask(Task task) {
		
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("insert into task (taskId, parentTaskId, taskName, parentTaskName)"
					+ " values (?,?, ?, ?)");
			preparedStatement.setInt(1, task.getTaskId());
			preparedStatement.setInt(2, task.getParentTaskId());
			preparedStatement.setString(3, task.getTaskName());
			preparedStatement.setString(4, task.getParentTaskName());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
	}

}
