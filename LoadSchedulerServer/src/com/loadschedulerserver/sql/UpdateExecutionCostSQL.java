package com.loadschedulerserver.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.loadschedulerserver.databaseconnection.DatabaseConnection;

public class UpdateExecutionCostSQL {
	
	private Connection connection = DatabaseConnection.getConnection();
	
	private static UpdateExecutionCostSQL updateExecutionCostSQL = null;
	
	private UpdateExecutionCostSQL() {
		
	}
	
	public static UpdateExecutionCostSQL getInstance() {
		if (updateExecutionCostSQL == null) {
			updateExecutionCostSQL = new UpdateExecutionCostSQL();
		}
		return updateExecutionCostSQL;
	}
	
	public void updateExecutionCost(String processorName, String taskName, double executionCost) {
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("update execution_cost set cost = ? where processor_id = ? and task_id = ?");
			preparedStatement.setDouble(1, executionCost);
			preparedStatement.setString(2, processorName);
			preparedStatement.setString(3, taskName);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
