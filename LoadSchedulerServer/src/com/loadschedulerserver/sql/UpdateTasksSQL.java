package com.loadschedulerserver.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;

import com.loadschedulerserver.databaseconnection.DatabaseConnection;
import com.loadschedulerserver.model.Task;

public class UpdateTasksSQL {

	private Connection connection = DatabaseConnection.connection;
	private static UpdateTasksSQL updateChildTasksSQL = null;
	
	private UpdateTasksSQL() {
		
	}
	
	public static UpdateTasksSQL getInstance() {
		if (updateChildTasksSQL == null) {
			updateChildTasksSQL = new UpdateTasksSQL();
		}
		return updateChildTasksSQL;
	}
	
	public void updateTasksTable(Task task, String processorId) {
		
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("update task set processor = ? , startTime = ?"
					+ " where taskId = ?");
			preparedStatement.setString(1, processorId);
			preparedStatement.setString(2, Calendar.getInstance().getTime().toString());
			preparedStatement.setInt(3, task.getTaskId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
}
