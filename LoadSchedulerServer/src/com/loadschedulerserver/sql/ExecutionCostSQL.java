package com.loadschedulerserver.sql;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.loadschedulerserver.databaseconnection.DatabaseConnection;


public class ExecutionCostSQL {
	
	private Connection connection = DatabaseConnection.getConnection();
	
	private static ExecutionCostSQL executionCostSQL = null;
	
	private ExecutionCostSQL() {
		
	}
	
	public static ExecutionCostSQL getInstance() {
		if (executionCostSQL == null) {
			executionCostSQL = new ExecutionCostSQL();
		}
		return executionCostSQL;
	}
	
	
	public void insertExecutionCost(String processorId, String taskId, int cost) {
		
		try {
			System.out.println("In insert query");
			PreparedStatement preparedStatement = connection.prepareStatement("insert into execution_cost (processor_id,"
					+ "task_id, cost) values (?,?,?)");
			preparedStatement.setString(1, processorId);
			preparedStatement.setString(2, taskId);
			preparedStatement.setDouble(3, cost);
			preparedStatement.executeUpdate();
			System.out.println("Inserted");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
