package com.loadschedulerserver.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.loadschedulerserver.allocator.PendingTaskAllocator;
import com.loadschedulerserver.databaseconnection.DatabaseConnection;

public class CheckChildTasksCompletionSQL {
	
	private Connection connection = DatabaseConnection.getConnection();
	
	private static CheckChildTasksCompletionSQL checkChildTasksCompletionSQL= null;
	
	private  CheckChildTasksCompletionSQL() {
		// TODO Auto-generated constructor stub
	}
	
	public static CheckChildTasksCompletionSQL getInstance() {
		if (checkChildTasksCompletionSQL == null) {
			checkChildTasksCompletionSQL = new CheckChildTasksCompletionSQL();
		}
		return checkChildTasksCompletionSQL;
	}
	
	public boolean checkPendingTask(int parentTaskId) {
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("select count(taskId) from task where "
					+ "parentTaskId = ? and endTime is null");
			preparedStatement.setInt(1, parentTaskId);
			int count = 0;
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				count = resultSet.getInt("count(taskId)");				
			}
			if (count == 0) {
				return false;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

}
