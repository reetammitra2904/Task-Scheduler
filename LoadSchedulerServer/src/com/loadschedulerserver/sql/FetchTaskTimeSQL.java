package com.loadschedulerserver.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import com.loadschedulerserver.databaseconnection.DatabaseConnection;

public class FetchTaskTimeSQL {
	
	private Connection connection = DatabaseConnection.getConnection();
	
	private static FetchTaskTimeSQL fetchTaskTimeSQL = null;
	
	private FetchTaskTimeSQL() {
		
	}
	
	public static FetchTaskTimeSQL getInstance() {
		if (fetchTaskTimeSQL == null) {
			fetchTaskTimeSQL = new FetchTaskTimeSQL();
		}
		return fetchTaskTimeSQL;
	}
	
	public ArrayList<Double> fetchTaskTime(String processorName, String taskName) {
		ArrayList<Double> taskTimeList = new ArrayList<Double>();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("select * from task where taskName = ? and processor = ?");
	        preparedStatement.setString(1, taskName);
	        preparedStatement.setString(2, processorName);
	        ResultSet resultSet = preparedStatement.executeQuery();
	        Calendar startTimeCalendar = Calendar.getInstance();
	        Calendar endTimeCalendar = Calendar.getInstance();
	        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);
	        while(resultSet.next()) {
	        	 endTimeCalendar.setTime(sdf.parse(resultSet.getString("endTime")));
	        	 startTimeCalendar.setTime(sdf.parse(resultSet.getString("startTime")));
	        	 double duration = (((Long)(endTimeCalendar.getTimeInMillis() - startTimeCalendar.getTimeInMillis())).doubleValue())/1000;
	        	 taskTimeList.add(duration);
	        }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return taskTimeList;
		
	}

}
