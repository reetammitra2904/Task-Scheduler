package com.loadschedulerserver.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.loadschedulerserver.databaseconnection.DatabaseConnection;

public class ProcessorCostSQL {
	
	private Connection connection = DatabaseConnection.getConnection();
	private static ProcessorCostSQL processorCostSQL = null;
	
	private ProcessorCostSQL() {
		
	}
	
	public static ProcessorCostSQL getInstance() {
		if (processorCostSQL == null) {
			processorCostSQL = new ProcessorCostSQL();
		}
		return processorCostSQL;
	}
	
	public double getCost(String processorId, String taskId) {
		double cost = 0;
		try {
			System.out.println("The task id is "+taskId+" and the processorId is "+processorId);
			PreparedStatement preparedStatement = connection.prepareStatement("select cost from execution_cost where "
					+ "task_id = ? and processor_id = ?");
			preparedStatement.setString(1, taskId);
			preparedStatement.setString(2, processorId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				cost = resultSet.getDouble("cost");
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cost;
	}
	
	}
