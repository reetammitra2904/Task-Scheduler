package com.loadschedulerserver.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.loadschedulerserver.databaseconnection.DatabaseConnection;

public class TruncateExecutionCostSQL {
	
	private Connection connection = DatabaseConnection.getConnection();
	
	private static TruncateExecutionCostSQL truncateExecutionCostSQL = null;
	
	private TruncateExecutionCostSQL() {
		
	}
	
	public static TruncateExecutionCostSQL getInstance() {
		if (truncateExecutionCostSQL == null) {
			truncateExecutionCostSQL = new TruncateExecutionCostSQL();
		}
		return truncateExecutionCostSQL;
	}
	
	public void truncateExecutionCostTable() {
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("truncate table execution_cost");
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	

}
