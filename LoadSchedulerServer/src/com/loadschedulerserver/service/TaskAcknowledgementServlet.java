package com.loadschedulerserver.service;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

import com.loadschedulerserver.allocator.PendingTaskAllocator;
import com.loadschedulerserver.allocator.TaskAllocator;
import com.loadschedulerserver.client.LoadSchedulerClient;
import com.loadschedulerserver.model.Task;
import com.loadschedulerserver.model.TaskCount;
import com.loadschedulerserver.sql.CheckChildTasksCompletionSQL;
import com.loadschedulerserver.sql.UpdateTaskCompletionSQL;
import com.loadschedulerserver.statistics.StatisticsCalculator;

public class TaskAcknowledgementServlet extends HttpServlet{
	
	public void doPost(HttpServletRequest request,
            HttpServletResponse response)
    throws ServletException, IOException
{
		ObjectMapper mapper = new ObjectMapper();
		String taskJSON = request.getParameter("task");
		Task task = mapper.readValue(taskJSON, Task.class);
		String processor = task.getProcessorId();
		int taskId = task.getTaskId();
		int parentTaskId = task.getParentTaskId();
		double cost = task.getCost();
		String parentTaskName = task.getParentTaskName();
		UpdateTaskCompletionSQL.getInstance().updateTaskCompletion(taskId);
		updateProcessorTaskCountMap(task.getProcessorId(), task.getTaskName());
        TaskAllocator.engagedProcessorMap.put(task.getProcessorId(), TaskAllocator.engagedProcessorMap.get(task.getProcessorId())- cost); 
        ArrayList<Task> taskList = TaskAllocator.processorTaskMap.get(processor);
        for (int index = 0; index < taskList.size(); index++) {
        	if (taskId == taskList.get(index).getTaskId()) {
        		taskList.remove(index);
        		break;
        	}
        	
        }
        TaskAllocator.processorTaskMap.put(processor, taskList);
		if (parentTaskId != 0 && isPendingTask(parentTaskId) && !CheckChildTasksCompletionSQL.getInstance().checkPendingTask(parentTaskId)) {
			boolean result = CheckChildTasksCompletionSQL.getInstance().checkPendingTask(parentTaskId);
			if (!result) {
				while (TaskAllocator.isProcessorsFree) {
					if (TaskAllocator.isFinalTask){
						break;
					}
					System.out.println("Waiting for isProcessorsFree to be false");
				}
				Task parentTask = getParentTask(parentTaskId);
				Thread pendingTaskAllocatorThread = new Thread(new PendingTaskAllocator(processor, parentTaskName, parentTaskId, parentTask));
				pendingTaskAllocatorThread.start();
				try {
					pendingTaskAllocatorThread.join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                Thread loadSchedulerClient = new Thread(new LoadSchedulerClient(processor, parentTask));
                loadSchedulerClient.start();
			}
		} else {
			if (TaskAllocator.engagedProcessorMap.get(processor) == 0) {
	        	TaskAllocator.engagedProcessorMap.remove(processor);
	        	TaskAllocator.freeProcessorsList.add(processor);
	        }
			
		}
		
} 
	
	private boolean isPendingTask(int parentTaskId) {
		ArrayList<Task> pendingTaskList = TaskAllocator.pendingTasksList;
		for (int index = 0; index < pendingTaskList.size(); index++) {
			if (parentTaskId == pendingTaskList.get(index).getTaskId()) {
				return true;
			}
			
		}
		return false;
	}
	
	private Task getParentTask(int parentTaskId) {
		ArrayList<Task> pendingTaskList = TaskAllocator.pendingTasksList;
		for (int index = 0; index < pendingTaskList.size(); index++) {
			if (parentTaskId == pendingTaskList.get(index).getTaskId()) {
				return pendingTaskList.get(index);
			}
			
		}
		return null;
	}
	
	private void updateProcessorTaskCountMap(String processorName, String taskName) {
		if (TaskAllocator.processorTaskCountMap.containsKey(processorName)) {
			ArrayList<TaskCount> taskCountList = TaskAllocator.processorTaskCountMap.get(processorName);
			boolean isTaskPresent = false;
			for (int index = 0; index < taskCountList.size(); index++) {
				if (taskName.equals(taskCountList.get(index).getTaskName())) {
					isTaskPresent = true;
					taskCountList.get(index).setCount(taskCountList.get(index).getCount()+1);
					TaskAllocator.processorTaskCountMap.put(processorName, taskCountList);
					if (taskCountList.get(index).getCount()%5 == 0) {						
						StatisticsCalculator.getInstance().updateStatistics(processorName, taskName, 0);
					}
				}
			}
			if (!isTaskPresent) {
				TaskCount taskCount = new TaskCount();
				taskCount.setCount(1);
				taskCount.setTaskName(taskName);
				taskCountList.add(taskCount);
				TaskAllocator.processorTaskCountMap.put(processorName, taskCountList);
			}
		} else {
			ArrayList<TaskCount> taskCountList = new ArrayList<TaskCount>();
			TaskCount taskCount = new TaskCount();
			taskCount.setTaskName(taskName);
			taskCount.setCount(1);
			taskCountList.add(taskCount);
			TaskAllocator.processorTaskCountMap.put(processorName, taskCountList);
		}
	}
	
	

}
