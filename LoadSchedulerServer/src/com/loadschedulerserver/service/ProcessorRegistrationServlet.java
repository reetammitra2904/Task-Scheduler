package com.loadschedulerserver.service;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

import com.loadschedulerserver.allocator.TaskAllocator;
import com.loadschedulerserver.model.Processor;
import com.loadschedulerserver.sql.ExecutionCostSQL;

public class ProcessorRegistrationServlet extends HttpServlet{
	
	public void doPost(HttpServletRequest request,
            HttpServletResponse response)
    throws ServletException, IOException
{
		String processorId = request.getRemoteAddr();
		String processorJson = request.getParameter("processor");
		System.out.println("The processor JSON is "+processorJson);
		ObjectMapper mapper = new ObjectMapper();
		Processor processor = mapper.readValue(processorJson, Processor.class);
		ArrayList<String> taskList = new ArrayList<String>(processor.getTaskCostMap().keySet());
		for (int index = 0; index < taskList.size(); index++) {
			ExecutionCostSQL.getInstance().insertExecutionCost(processorId, taskList.get(index), 
					processor.getTaskCostMap().get(taskList.get(index)));
			
		}
		TaskAllocator.freeProcessorsList.add(processorId);
}
	
	

}
