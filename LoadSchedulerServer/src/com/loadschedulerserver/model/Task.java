package com.loadschedulerserver.model;

import java.util.ArrayList;
import java.util.Calendar;

public class Task {
	
	private String taskName;
	private int taskId;
	private ArrayList<String> dependentTasks;
	private int parentTaskId;
	private String parentTaskName;
	private double cost;
	private String processorId;
	private Calendar time;

	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public int getTaskId() {
		return taskId;
	}
	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}
	public ArrayList<String> getDependentTasks() {
		return dependentTasks;
	}
	public void setDependentTasks(ArrayList<String> dependentTasks) {
		this.dependentTasks = dependentTasks;
	}
	public int getParentTaskId() {
		return parentTaskId;
	}
	public void setParentTaskId(int parentTaskId) {
		this.parentTaskId = parentTaskId;
	}
	public String getParentTaskName() {
		return parentTaskName;
	}
	public void setParentTaskName(String parentTaskName) {
		this.parentTaskName = parentTaskName;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public String getProcessorId() {
		return processorId;
	}
	public void setProcessorId(String processorId) {
		this.processorId = processorId;
	}
	public Calendar getTime() {
		return time;
	}
	public void setTime(Calendar time) {
		this.time = time;
	}
	
	

}
